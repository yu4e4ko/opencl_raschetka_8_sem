__kernel void calculateIndexes(__global const int* sourceArray, __global const int* data, __global int* indexes)
{
	int currentThread = get_global_id(0);

	int length = data[0];
	int startIndex = data[1];
	int endIndex = data[2];

	int threadCount = data[3];
	int currentIndex = 0;
	int lastIndex = length;

	int currentSourceElement = 0;

	if(threadCount >= length)
	{
		currentIndex = get_global_id(0);

		if(currentIndex < startIndex)
		{
			return;
		}
		
		if(lastIndex > endIndex)
		{
			lastIndex = endIndex;
		}

		currentSourceElement = sourceArray[currentIndex];

		for(int i = currentIndex + 1; i < lastIndex; i++){
			if(sourceArray[i] > currentSourceElement){
				indexes[currentIndex] = i;
				return;
			}
		}
	}
	else
	{
		float offsetDouble = (float) length / threadCount;

		int offset = (offsetDouble > 0.0) ? (offsetDouble + 0.5) : (offsetDouble - 0.5);

		currentIndex = offset * currentThread;
		lastIndex = currentIndex + offset;

		if (threadCount - 1 == currentThread)
		{
			lastIndex = length;
		}

		if (lastIndex > endIndex)
		{
			lastIndex = endIndex;
		}

		
		if (currentIndex < startIndex)
		{
			currentIndex = startIndex;
		}
		
		if(lastIndex < currentIndex)
		{
			return;
		}

		for (int sourceElement = currentIndex; sourceElement < lastIndex; sourceElement++)
		{
			currentSourceElement = sourceArray[sourceElement];
			
			for (int i = sourceElement + 1; i < length; i++)
			{
				if (sourceArray[i] > currentSourceElement)
				{
					indexes[sourceElement] = i;
					break;
				}
			}
		}
		
		//printf("Thread: %d; startIndex: %d; lastIndex1: %d\n", currentThread, currentIndex, lastIndex);
	}	
}