#include "main.h"

#include <stdio.h>
#include <stdlib.h>
#include <CL/opencl.h>
#include <math.h>
#include <fstream>

void randomInit(int source[], const unsigned length) {
	unsigned i;
	for (i = 0; i < length; i++) {
		source[i] = rand() % 20;
	}
}

void initIndexes(int indexes[], const unsigned length){
	unsigned i;
	for (i = 0; i < length; i++) {
		indexes[i] = length;
	}
}

int getNumberLength(int number){
	if (number == 0){
		return 1;
	}

	int nDigits = floor(log10(abs((double)number))) + 1;

	if (number < 0){
		nDigits++;
	}

	return nDigits;
}

void printHeaderFooter(){
	printf("----------|----------|\n");
	printf("Source    |Result    |\n");
	printf("----------|----------|\n");
}

void printCell(int number){
	const unsigned cellLength = 10;
	int numberLength;
	printf("%d", number);
	numberLength = getNumberLength(number);

	for (int j = 0; j < cellLength - numberLength; j++){
		printf(" ");
	}
	printf("|");
}

void printResult(int source[], int result[], const unsigned length){
	printHeaderFooter();

	unsigned i;
	for (i = 0; i < length; i++) {
		printCell(source[i]);
		printCell(result[i]);
		printf("\n");
	}

	printHeaderFooter();
	printf("\n");
}

char* getProgramSource(){
	std::fstream kernelFile;
	std::string kernelPath = "";
	kernelPath.append("calculateIndexes.cl");
	kernelFile.open(kernelPath.c_str(), std::ios::binary | std::ios::in);
	if (!kernelFile.is_open())
	{
		printf("Failed to load kernel file: %s", kernelPath);
	}

	kernelFile.seekg(0, std::ios::end);
	unsigned int length = kernelFile.tellg();
	char * code = new char[length + 1];
	kernelFile.seekg(0, std::ios::beg);
	kernelFile.read(code, length);
	code[length] = '\0';
	
	return code;
}

int main(int argc, char ** argv) {
	const bool isPrintResult = false;

	const int length = 30000;
	const int sIndex = 0;
	const int eIndex = 30000;

	const size_t threadCount = 1;
	const unsigned startIndex = sIndex;
	const unsigned endIndex = eIndex;

	const int dataArrayLength = 4;
	int * sourceArray = new int[length];
	int * sourceIndexes = new int[length];
	int * dataArray = new int[dataArrayLength] { length, sIndex, eIndex, (int)threadCount };

	initIndexes(sourceIndexes, length);

	// manual init
	//sourceArray = new int[length] { 0, 1, 2, 5, 4, 2 };
	
	// random init
	randomInit(sourceArray, length);

#define PROCESS_ERROR(msg,err) if (err != CL_SUCCESS) { \
		printf ((msg),(err));\
		return (err); \
		}

	cl_int err;
	cl_uint numPlatforms;
	cl_platform_id platform = NULL;        // default system platform

	err = clGetPlatformIDs(0, NULL, &numPlatforms);

	if (err == CL_SUCCESS) {
		cl_platform_id* platforms = new cl_platform_id[numPlatforms];
		err = clGetPlatformIDs(numPlatforms, platforms, NULL);

		if (err == CL_SUCCESS) {
			platform = platforms[numPlatforms -1];
			//platform = platforms[0];
		}

		PROCESS_ERROR("Platform detection faults. Error Code: %d.", err);
	}
	PROCESS_ERROR("Platform detection faults. Error Code: %d.", err);

	char platformVendor[1024];
	err = clGetPlatformInfo(platform, CL_PLATFORM_VENDOR, sizeof(platformVendor), platformVendor, NULL);
	PROCESS_ERROR("Platform detection faults. Error Code: %d.", err);

	// create OpenCL device & context
	cl_device_id device_id;             // compute device id 
	cl_uint deviceCount = 0;
	err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &deviceCount);
	PROCESS_ERROR("Device detection faults. Error Code: %d.", err);
	err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, deviceCount, &device_id, NULL);
	PROCESS_ERROR("Device detection faults. Error Code: %d.", err);

	char deviceName[1024];
	clGetDeviceInfo(device_id, CL_DEVICE_NAME, sizeof(deviceName), &deviceName, NULL);

	cl_context hContext = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	PROCESS_ERROR("Context creation faults. Error Code: %d.", err);

	size_t nContextDescriptorSize;
	err = clGetContextInfo(hContext, CL_CONTEXT_DEVICES,
		0, 0, &nContextDescriptorSize);
	PROCESS_ERROR("Context creation faults. Error Code: %d.", err);

	cl_device_id * aDevices = (cl_device_id *)malloc(nContextDescriptorSize);
	err = clGetContextInfo(hContext, CL_CONTEXT_DEVICES,
		nContextDescriptorSize, aDevices, 0);
	PROCESS_ERROR("Context creation faults. Error Code: %d.", err);

	// create a command queue for first device the context reported
	cl_command_queue hCmdQueue;
	hCmdQueue = clCreateCommandQueue(hContext, aDevices[0], CL_QUEUE_PROFILING_ENABLE, 0);

	// create & compile program
	const char * source = getProgramSource();
	
	cl_program hProgram;
	hProgram = clCreateProgramWithSource(hContext, 1, &source, 0, 0);
	err = clBuildProgram(hProgram, 0, 0, 0, 0, 0);
	PROCESS_ERROR("Program building faults. Error Code: %d.", err);

	// create kernel
	cl_kernel hKernel;
	hKernel = clCreateKernel(hProgram, "calculateIndexes", 0);

	// allocate device memory
	cl_mem hDeviceMemSourceArray, hDeviceMemResult, hDeviceMemData;

	hDeviceMemSourceArray = clCreateBuffer(hContext,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		length * sizeof(cl_int),
		sourceArray,
		0);

	hDeviceMemData = clCreateBuffer(hContext,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		dataArrayLength * sizeof(cl_int),
		dataArray,
		0);

	hDeviceMemResult = clCreateBuffer(hContext,
		CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
		length * sizeof(cl_int),
		sourceIndexes,
		0);

	// setup parameter values
	err = clSetKernelArg(hKernel, 0, sizeof(cl_mem), (void *)&hDeviceMemSourceArray);
	PROCESS_ERROR("First argument failure. Error Code: %d.", err);

	err = clSetKernelArg(hKernel, 1, sizeof(cl_mem), (void *)&hDeviceMemData);
	PROCESS_ERROR("Second argument failure. Error Code: %d.", err);

	err = clSetKernelArg(hKernel, 2, sizeof(cl_mem), (void *)&hDeviceMemResult);
	PROCESS_ERROR("Third argument failure. Error Code: %d.", err);

	// execute kernel
	cl_event myEvent;
	cl_ulong startTime, endTime;

	err = clEnqueueNDRangeKernel(hCmdQueue, hKernel, 1, 0, &threadCount, 0, 0, 0, &myEvent);
	PROCESS_ERROR("NDRange failure. Error Code: %d.", err);

	err = clWaitForEvents(1, &myEvent);
	PROCESS_ERROR("clWaitForEvents error: %d.", err);

	err = clGetEventProfilingInfo(myEvent, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &startTime, NULL);
	PROCESS_ERROR("CL_PROFILING_COMMAND_START: %d.", err);
	err = clGetEventProfilingInfo(myEvent, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &endTime, NULL);
	PROCESS_ERROR("CL_PROFILING_COMMAND_END: %d.", err);

	err = clEnqueueReadBuffer(hCmdQueue, hDeviceMemResult, CL_TRUE, 0,
		length * sizeof(cl_int),
		sourceIndexes, 0, 0, 0);
	PROCESS_ERROR("Buffer failure. Error Code: %d.", err);

	// print results
	if (isPrintResult){
		printResult(sourceArray, sourceIndexes, length);
	}

	printf("Source array length: %d\n", length);

	cl_ulong alltime = endTime - startTime;
	printf("\n");
	printf("Time: %.10lf seconds", double(alltime / 1000000000.0));
	printf("\n\n");

	printf("Device: %s\n\n", deviceName);

	// Release memory
	err = clReleaseMemObject(hDeviceMemSourceArray);
	PROCESS_ERROR("Release failure. Error Code: %d.", err);

	err = clReleaseMemObject(hDeviceMemResult);
	PROCESS_ERROR("Release failure. Error Code: %d.", err);

	err = clReleaseMemObject(hDeviceMemData);
	PROCESS_ERROR("Release failure. Error Code: %d.", err);

	err = clReleaseProgram(hProgram);
	PROCESS_ERROR("Release failure. Error Code: %d.", err);

	err = clReleaseKernel(hKernel);
	PROCESS_ERROR("Release failure. Error Code: %d.", err);

	err = clReleaseCommandQueue(hCmdQueue);
	PROCESS_ERROR("Release failure. Error Code: %d.", err);

	err = clReleaseContext(hContext);
	PROCESS_ERROR("Release failure. Error Code: %d.", err);

	// FINALIZE BLOCK
	delete[] sourceArray;
	delete[] sourceIndexes;
	delete[] dataArray;

	return 0;
}